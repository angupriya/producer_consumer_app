# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...




Steps to run producer consumer app
1. from root folder docker-compose build;
2. from root folder docker-compose up;
3. localhost:3001/producer  //to produce message
4. rake consumer:test //to consume message, inside consumer docker container
5. localhost:3000 //to display the consumed message in consumer app
6. docker-compose down; After stopping container 