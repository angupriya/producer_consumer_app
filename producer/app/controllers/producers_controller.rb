class ProducersController < ActionController::Base

	def create
		@title = params["title"]
		require "kafka"
	  	kafka = Kafka.new(["kafka:9092"], client_id: "my-application")
	  	kafka.deliver_message(@title, topic: "new_greetings")
	end
end
