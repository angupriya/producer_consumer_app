namespace :consumer do
  desc "TODO"
  task test: :environment do
  	require "kafka"
  	kafka = Kafka.new(["kafka:9092"])
  	consumer = kafka.consumer(group_id: "my-consumer")
  	consumer.subscribe("new_greetings")
  	consumer.each_message do |message|
	  ActionCable.server.broadcast("room_channel",{content: message.value})
	end
  end
end